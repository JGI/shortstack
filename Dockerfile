FROM debian:jessie

RUN set -ex \
&&  apt-get update -y \
&&  apt-get install -y \
    bash \
    ca-certificates \
    g++ \
    gcc \
    git \
    gzip \
    libgomp1 \
    libpython2.7 \
    libgsl0ldbl \
    lbzip2 \
    perl \
    python \
    make \
    ncurses-dev \
    tar \
    wget \
    zip \
    zlib1g-dev

RUN mkdir /vienna_rna/ \
&&  mkdir /bowtie/ \
&&  mkdir /samtools/ \
&&  mkdir /shortstack/

ENV BOWTIE_VERSION=1.1.2
# downloading bowtie...
RUN cd /bowtie/ \
&& wget --no-check-certificate https://sourceforge.net/projects/bowtie-bio/files/bowtie/${BOWTIE_VERSION}/bowtie-${BOWTIE_VERSION}-linux-x86_64.zip

# unpacking bowtie...
RUN cd /bowtie/ \
&& unzip bowtie-${BOWTIE_VERSION}-linux-x86_64.zip
ENV PATH=/bowtie/bowtie-${BOWTIE_VERSION}/:$PATH

ENV VIENNA_RNA_VERSION=2.4.1-1
# downloading vienna RNA...
RUN cd /vienna_rna/ \
&& wget https://www.tbi.univie.ac.at/RNA/download/debian/debian_8_0/viennarna_${VIENNA_RNA_VERSION}_amd64.deb \
&& dpkg -i /vienna_rna/viennarna_${VIENNA_RNA_VERSION}_amd64.deb

ENV SAMTOOLS_VERSION=1.4
# downloading samtools...
RUN cd /samtools/ \
&& wget --no-check-certificate https://sourceforge.net/projects/samtools/files/samtools/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2 \
&& tar xvf samtools-${SAMTOOLS_VERSION}.tar.bz2

# compiling samtools...
# samtools:
RUN cd /samtools/samtools-${SAMTOOLS_VERSION}/ \
&&  ./configure --disable-bz2 --disable-lzma\
&&  make \
&&  make prefix=/samtools/samtools/ install
ENV PATH=/samtools/samtools/bin:$PATH 

ENV SHORTSTACK_VERSION=v3.8.3
# downloading ShortStack...
RUN cd /shortstack/ \
&& git clone https://github.com/MikeAxtell/ShortStack.git \
&& cd ./ShortStack \
&& git checkout tags/${SHORTSTACK_VERSION}
ENV PATH=/shortstack/ShortStack/:$PATH

CMD ["/bin/bash"]

